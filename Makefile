JS_SRC ?= src
JS_ESLINT_CONFIG ?= .eslintrc.json

js-install:
	@npm install --no-cache

js-format: js-install
	@npx eslint -c $(JS_ESLINT_CONFIG) \
		--fix \
		--ignore-pattern "**/node_modules/*" --ignore-pattern "**/.eslintignore" \
		"$(JS_SRC)/**/*.{js,jsx,ts,tsx}"

js-lint: js-install
	@mkdir -p build/reports
	@npm list --depth=0 --json > build/reports/dependencies.json
	@npm list --depth=0  > build/reports/dependencies.txt
	@npx eslint -c $(JS_ESLINT_CONFIG) \
		--fix-dry-run \
		--ignore-pattern "**/node_modules/*" --ignore-pattern "**/.eslintignore" \
		"$(JS_SRC)/**/*.{js,jsx,ts,tsx}"
	@npx eslint -c $(JS_ESLINT_CONFIG) \
		--ignore-pattern "**/node_modules/*" --ignore-pattern "**/.eslintignore" \
		-f junit -o build/reports/linting.xml \
		"$(JS_SRC)/**/*.{js,jsx,ts,tsx}"

js-test: js-install
	@mkdir -p build/reports; \
	export JEST_JUNIT_OUTPUT_DIR=build/reports; \
	npx react-scripts test --ci --env=jsdom --watchAll=false --passWithNoTests \
	--verbose --reporters=default --reporters=jest-junit \
	--coverage --coverageDirectory=build/reports \
	--logHeapUsage --runInBand

build-docs:
	docker run --rm -d -v $(PWD):/tmp -w /tmp/docs netresearch/sphinx-buildbox sh -c "make html"

search-string:
	grep -rnw '.' --exclude-dir=node_modules --exclude-dir=docs --exclude-dir=build -e '$(STR)'