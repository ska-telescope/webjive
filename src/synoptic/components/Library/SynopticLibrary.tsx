import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Files from "react-files";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faFileUpload, faFile } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NotLoggedIn from "../../../jive/components/DeviceViewer/NotLoggedIn/NotLoggedIn";
import { getIsLoggedIn } from "../../../shared/user/state/selectors";
import { getSynoptics, getSelectedSynoptic } from "../../state/selectors";
import { IRootState } from "../../../shared/state/reducers/rootReducer";
import { deleteSynoptic } from "../../state/actionCreators";
import DeleteSynopticModal from "../modals/DeleteSynopticModal";
import { Synoptic, SelectedSynoptic, SharedSynoptics } from "../../types";
import {
  loadSynoptic,
  saveSynoptic,
  exportSynoptic,
  importSynoptic,
} from "../../state/actionCreators";
import { getGroupSynoptics, getGroupSynopticCount } from "../../synopticRepo";

import TangoAPI from "../../../shared/api/tangoAPI";
import "./SynopticLibrary.css";
library.add(faFileUpload, faFile);

interface Props {
  tangoDB: string;
  render: boolean;
  synoptics: Synoptic[];
  isLoggedIn: boolean;
  selectedSynoptic: SelectedSynoptic;
  onDeleteSynoptic: (id: string) => void;
  loadSynoptic: (id: string) => void;
  exportSynoptic: (id: string) => void;
  saveSynoptic: (id: string, name: string, variables: []) => void;
  onUploadFile: () => void;
  onFilesChange: (files) => void;
}

interface State {
  deleteSynopticModalId: string;
  configSynopticModalId: string;
  expandedGroups: { [group: string]: boolean };
  sharedSynoptics: SharedSynoptics;
  tangoClasses: [];
}

class SynopticLibrary extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.handleDeleteSynoptic = this.handleDeleteSynoptic.bind(this);

    this.state = {
      deleteSynopticModalId: "",
      configSynopticModalId: "",
      expandedGroups: {},
      sharedSynoptics: {
        synoptics: [],
        availableGroupSynoptics: {},
      },
      tangoClasses: [],
    };
  }

  public componentDidMount() {
    this.loadGroupSynopticCount();
    this.fetchClasses();
  }

  public async componentDidUpdate(prevProp) {
    //if we just logged in, fetch synoptic count
    if (this.props.isLoggedIn && !prevProp.isLoggedIn) {
      this.loadGroupSynopticCount();
    }
  }

  public async loadGroupSynopticCount() {
    const meta = await getGroupSynopticCount();
    const keys = Object.keys(meta);
    const sharedSynoptics: SharedSynoptics = {
      synoptics: [],
      availableGroupSynoptics: {},
    };
    keys.forEach((key) => {
      sharedSynoptics.availableGroupSynoptics[key] = {
        count: meta[key],
        loaded: false,
      };
    });
    this.setState({ sharedSynoptics });
  }

  onSharedSynopticLoad = (sharedSynoptics: SharedSynoptics) =>
    this.setState({ sharedSynoptics });

  /**
   * This is called on config btn click, loads the synoptic variables from redux & opens the modal with this content
   *
   * @param synopticId
   */
  handleConfigBtnClick = (synopticId: string) => {
    this.setState({ configSynopticModalId: synopticId });
    // const synoptic = this.props.synoptics.find(
    //   (synoptic) => synoptic.id === synopticId
    // );
  };

  public render() {
    if (!this.props.render) {
      return null;
    }
    const { synoptics, isLoggedIn } = this.props;
    const {
      synoptics: groupSynoptics,
      availableGroupSynoptics,
    } = this.state.sharedSynoptics;
    const groupsWithSharedSynoptics = Object.keys(
      availableGroupSynoptics
    ).filter((group) => availableGroupSynoptics[group].count > 0);

    if (!isLoggedIn) {
      return (
        <NotLoggedIn>
          You have to be logged in to view and manage your synoptics.
        </NotLoggedIn>
      );
    }
    return (
      <div className="synoptic-settings">
        <div className="synoptic-settings-title">Add Synoptic</div>
        <div className="synoptic-row synoptic-menu">
          <Files
            onChange={this.props.onFilesChange}
            onError={this.onFilesError}
            accepts={[".svg"]}
            multiple={false}
            maxFileSize={10000000}
            minFileSize={0}
            clickable
          >
            <button
              className="synoptic-menu-button"
              title="Import an existing synoptic from a file (*.wj)"
            >
              <FontAwesomeIcon icon="file-upload" />

              <span className="synoptic-menu-title">Import synoptic</span>
            </button>
          </Files>
        </div>
        <div className="synoptic-settings-title">My Synoptics</div>

        {synoptics.map((synoptic) => {
          return this.SynopticRow(synoptic, false);
        })}
        {/* SHARED SYNOPTICS */}
        <div className="synoptic-settings-title" style={{ marginTop: "0.5em" }}>
          Shared Synoptics
        </div>

        {groupsWithSharedSynoptics.map((groupName) => {
          return (
            <Fragment key={groupName}>
              {this.groupSynopticTitle(
                groupName,
                availableGroupSynoptics[groupName].count,
                this.state.expandedGroups[groupName]
              )}
              {this.state.expandedGroups[groupName] &&
                groupSynoptics
                  .filter((synoptic) => synoptic.group === groupName)
                  .map((synoptic) => {
                    return this.SynopticRow(synoptic, true);
                  })}
            </Fragment>
          );
        })}
        {groupsWithSharedSynoptics.length === 0 && (
          <div style={{ fontStyle: "italic", padding: "0.5em" }}>
            There are no shared synoptics in any of your groups
          </div>
        )}
      </div>
    );
  }
  groupSynopticTitle = (
    groupName: string,
    count: number,
    expanded: boolean
  ) => {
    if (expanded) {
      return (
        <div
          style={{ cursor: "pointer" }}
          onClick={() => this.collapseGroup(groupName)}
          className="synoptic-settings-title subtitle"
        >
          {groupName} ({count})
          <FontAwesomeIcon
            style={{ marginLeft: "0.5em" }}
            icon="chevron-up"
          />{" "}
        </div>
      );
    }
    return (
      <div
        style={{ cursor: "pointer" }}
        onClick={() => this.expandGroup(groupName)}
        title="Load synoptics shared with this group"
        className="synoptic-settings-title subtitle"
      >
        {groupName} ({count})
        <FontAwesomeIcon
          style={{ marginLeft: "0.5em" }}
          icon="chevron-down"
        />{" "}
      </div>
    );
  };

  expandGroup = async (groupName: string) => {
    const { availableGroupSynoptics, synoptics } = this.state.sharedSynoptics;
    const loaded = availableGroupSynoptics[groupName].loaded;
    let groupSynoptics: Synoptic[] = [];
    if (!loaded) {
      groupSynoptics = await getGroupSynoptics(groupName);
      availableGroupSynoptics[groupName].loaded = true;
      synoptics.push(...groupSynoptics);
      this.setState({
        sharedSynoptics: {
          availableGroupSynoptics,
          synoptics: synoptics.filter(
            (value, index, self) => self.indexOf(value) === index
          ),
        },
      });
    }
    const { expandedGroups } = this.state;
    this.setState({ expandedGroups: { ...expandedGroups, [groupName]: true } });
  };
  collapseGroup = (groupName: string) => {
    const { expandedGroups } = this.state;
    this.setState({
      expandedGroups: { ...expandedGroups, [groupName]: false },
    });
  };
  setSelectedSynoptic = (id: string) => {
    if (id) {
      this.props.loadSynoptic(id);
    }
  };
  SynopticRow = (synoptic: Synoptic, shared: boolean) => {
    const { id: selectedSynopticId } = this.props.selectedSynoptic;
    return (
      <Fragment key={synoptic.id}>
        <div
          className={
            "synoptic-row " +
            (synoptic.id === selectedSynopticId ? "selected" : "")
          }
        >
          {selectedSynopticId !== synoptic.id ? (
            <button
              onClick={() => this.setSelectedSynoptic(synoptic.id)}
              className="synoptic-link"
            >
              {synoptic.name || "Untitled synoptic"}
            </button>
          ) : (
            <span>{synoptic.name || "Untitled synoptic"}</span>
          )}
          <div
            style={{
              width: "6.5em",
              textAlign: "right",
              alignSelf: "flex-start",
            }}
          >
            {!shared && (
              <div id="buttons">
                <button
                  title={`Delete synoptic '${synoptic.name ||
                    "Untitled synoptic"}'`}
                  className="delete-button"
                  onClick={() =>
                    this.setState({ deleteSynopticModalId: synoptic.id })
                  }
                >
                  <FontAwesomeIcon icon="trash" />
                </button>

                <button
                  title={`Export synoptic '${synoptic.name ||
                    "Untitled synoptic"}'`}
                  className="delete-button"
                  onClick={() => this.props.exportSynoptic(synoptic.id)}
                >
                  <FontAwesomeIcon icon="arrow-alt-circle-down" />
                </button>
              </div>
            )}
            {shared && (
              <span
                title={"This synoptic is owned by " + synoptic.user}
                style={{
                  color: "#666",
                  fontSize: "0.8em",
                  fontStyle: "italic",
                }}
              >
                <FontAwesomeIcon icon="user" /> {synoptic.user}
              </span>
            )}
          </div>
        </div>

        {this.state.deleteSynopticModalId === synoptic.id && (
          <DeleteSynopticModal
            id={synoptic.id}
            name={synoptic.name}
            onClose={() => this.setState({ deleteSynopticModalId: "" })}
            onDelete={this.handleDeleteSynoptic}
          />
        )}
      </Fragment>
    );
  };

  private handleDeleteSynoptic(id: string) {
    this.props.onDeleteSynoptic(id);
    this.setState({ deleteSynopticModalId: "" });
  }

  private onFilesError = (error, file) => {
    console.log("error code " + error.code + ": " + error.message);
  };

  async fetchClasses() {
    const { tangoDB } = this.props;
    const data = await TangoAPI.fetchSelectedClassesAndDevices(tangoDB, []);

    this.setState({ tangoClasses: data });
  }
}
function mapStateToProps(state: IRootState) {
  return {
    synoptics: getSynoptics(state),
    isLoggedIn: getIsLoggedIn(state),
    selectedSynoptic: getSelectedSynoptic(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    saveSynoptic: (id: string, name: string, variables: []) =>
      dispatch(saveSynoptic(id, name, variables)),
    onDeleteSynoptic: (id: string) => dispatch(deleteSynoptic(id)),
    loadSynoptic: (id: string) => dispatch(loadSynoptic(id)),
    exportSynoptic: (id: string) => dispatch(exportSynoptic(id)),
    onUploadFile: () => {},
    onFilesChange: (files) => {
      if (files[0] !== undefined) {
        dispatch(importSynoptic(files[0]));
      }
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SynopticLibrary);
