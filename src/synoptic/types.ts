import { NotificationLevel } from "../shared/notifications/notifications";

export interface BaseInputDefinition<T> {
  label?: string;
  repeat?: boolean;
  default?: T;
  required?: boolean;
  title?: string;
}

export interface Notification {
  level: NotificationLevel;
  sourceAction: string;
  msg: string;
}

export interface BooleanInputDefinition extends BaseInputDefinition<boolean> {
  type: "boolean";
}

export interface RadioInputDefinition extends BaseInputDefinition<boolean> {
  type: "radio";
}

export interface NumberInputDefinition extends BaseInputDefinition<number> {
  type: "number";
  nonNegative?: boolean;
}

export interface StringInputDefinition extends BaseInputDefinition<string> {
  type: "string";
  renderAs?: "text" | "textarea";
  placeholder?: string;
}
export interface StyleInputDefinition extends BaseInputDefinition<string> {
  type: "style";
}

interface SelectInputDefinitionOption<T> {
  name: string;
  value: T; // ?
}

export interface SelectInputDefinition<T = string>
  extends BaseInputDefinition<string> {
  type: "select";
  options: SelectInputDefinitionOption<T>[];
}

export interface VariableInputDefinition<T = string>
  extends BaseInputDefinition<string> {
  type: "variable";
  options?: SelectInputDefinitionOption<T>[];
}

export interface ColorInputDefinition extends BaseInputDefinition<string> {
  type: "color";
}

export type InputDefinition =
  | BooleanInputDefinition
  | RadioInputDefinition
  | NumberInputDefinition
  | StringInputDefinition
  | SelectInputDefinition
  | VariableInputDefinition
  | ColorInputDefinition
  | StyleInputDefinition;

export interface InputDefinitionMapping {
  [name: string]: InputDefinition;
}

export interface Widget {
  _id: string; //not really used, but automatically created by mongodb
  type: string;
  id: string;
  valid: boolean;
  canvas: string;
  x: number;
  y: number;
  width: number;
  height: number;
  inputs: InputMapping;
  innerWidgets?: Widget[];
  [percentage: string]: any;
  order: number;
}

//meta information about available group synoptics, with count and whether it has been loade for each group
export interface AvailableGroupSynoptics {
  [group: string]: {
    count: number;
    loaded: boolean;
  };
}
export interface SharedSynoptics {
  synoptics: Synoptic[];
  availableGroupSynoptics: AvailableGroupSynoptics;
}
export interface SynopticEditHistory {
  undoActions: Record<string, Widget>[];
  redoActions: Record<string, Widget>[];
  undoIndex: number;
  redoIndex: number;
  undoLength: number;
  redoLength: number;
}

export interface Synoptic {
  id: string;
  name: string;
  user: string;
  insertTime: Date | null;
  updateTime: Date | null;
  group: string | null;
  groupWriteAccess: boolean;
  lastUpdatedBy: string | null;
  selectedIds: string[];
  history: SynopticEditHistory;
  variables: Variable[];
}

export interface SelectedSynoptic extends Synoptic {
  svg: string;
  selectedIds: string[];
  history: SynopticEditHistory;
}

export interface Variable {
  _id?: string;
  name: string;
  class: string;
  device: string;
}

export interface InputMapping {
  [name: string]: any;
}

export type IndexPath = Array<string | number>;

export interface Canvas {
  id: string;
  name: string;
}
