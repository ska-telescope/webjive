import React from "react";
import "../../tests/globalMocks";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import { configure, mount } from "enzyme";
import { Provider } from "react-redux";
import { MemoryRouter, Route, Switch } from "react-router";

import configureStore from "../../../shared/state/store/configureStore";
import { NavContainer, Navbar } from "./Navbar";

configure({ adapter: new Adapter() });

let loadStateUrls = jest.fn();
let saveStateUrls = jest.fn();
let onLoadTangoDBName = jest.fn();
let fetchCommunicationHealth = jest.fn();

describe("Test Navbar", () => {
  const store = configureStore();

  it("renders without crashing", () => {
    let sampleStateUrl = {
      devices: "/testdb/devices",
      dashboard: "/testdb/dashboard",
      deviceUrl: "http://localhost:22484/testdb/devices/sys/tg_test",
      dashboardUrl: "http://localhost:22484/testdb/dashboard",
      synopticUrl: "http://localhost:22484/testdb/synoptic",
    };

    const element = React.createElement(Navbar.WrappedComponent, {
      stateUrls: sampleStateUrl,
      mode: "edit",
      onLoadStateUrls: loadStateUrls,
      onSaveStateUrls: saveStateUrls,
      onLoadTangoDBName: onLoadTangoDBName,
      fetchCommunicationHealth: fetchCommunicationHealth
    });

    const content = mount(<Provider store={store}>{element}</Provider>);

    content.setProps({
      username: "CREAM",
      stateUrls: sampleStateUrl,
      onLoadStateUrls: jest.fn(),
      onSaveStateUrls: jest.fn(),
      onLoadTangoDBName: jest.fn(),
    });

    expect(content.find(".navigation").length).toEqual(1);
  });

  it("test Navbar with Memory Router for Navbar & NavContainer", async () => {
    const config = window["config"];
    config.synopticEnabled = false;
    const wrapper = mount(
      <MemoryRouter initialEntries={["/testdb/devices"]}>
        <Provider store={store}>
          <Switch>
            <Route path="/testdb/devices" render={() => <Navbar />} />
          </Switch>
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper.find(Navbar)).toHaveLength(1);

    let params = {
      section: "dashboard",
      tangoDB: "testdb",
    };
    let stateUrls = {
      deviceUrl: "",
      dashboardUrl: "",
      synopticUrl: "",
    };
    const wrapper1 = mount(
      <MemoryRouter initialEntries={["/testdb/devices"]}>
        <Provider store={store}>
          <Switch>
            <Route
              path="/testdb/devices"
              render={() => (
                <NavContainer
                  params={params}
                  saveStateUrls={saveStateUrls}
                  stateUrls={stateUrls}
                />
              )}
            />
          </Switch>
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper1.find(NavContainer)).toHaveLength(1);
    const NavHtml = wrapper1.find(NavContainer).html();
    expect(NavHtml).toContain("non-active");
    expect(NavHtml).toContain("active");
    expect(NavHtml).toContain("pagelinks");
    expect(NavHtml).toContain("tabbed-menu");
    expect(NavHtml).toContain("Devices");
    expect(NavHtml).toContain("Dashboards");
    expect(NavHtml).not.toContain("Synoptics");

    wrapper1
      .find(NavContainer)
      .find(".tabbed-menu.active")
      .at(0)
      .simulate("click");
    expect(saveStateUrls).toHaveBeenCalledTimes(1);

    const nonActiveElements = wrapper1.find(NavContainer).find(".non-active");
    expect(nonActiveElements.length).toBe(1);

    config.synopticEnabled = true;

    wrapper1.setProps({
      params: {
        section: "synoptic",
        tangoDB: "testdb",
      },
    });

    wrapper1.update();

    const NavHtml1 = wrapper1.find(NavContainer).html();
    expect(NavHtml1).toContain("non-active");
    expect(NavHtml1).toContain("active");
    expect(NavHtml1).toContain("pagelinks");
    expect(NavHtml1).toContain("tabbed-menu");
    expect(NavHtml1).toContain("Devices");
    expect(NavHtml1).toContain("Dashboards");
    expect(NavHtml1).toContain("Synoptics");

    wrapper1
      .find(NavContainer)
      .find(".tabbed-menu.active")
      .at(0)
      .simulate("click");

    expect(saveStateUrls).toHaveBeenCalledTimes(2);
  });
});