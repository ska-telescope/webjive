import { GraphQLClient } from 'graphql-request'
import { EXECUTE_COMMAND } from './graphqlQuery';

export default class UserActions {

    private url: string;

    constructor(url: string) {
        this.url = url;
    }

    createGraphqlClient(headers = {}) {
        const graphQLClient = new GraphQLClient(this.url, {
            headers: headers
        })

        return graphQLClient;
    }

    createQuery(query: string, variables = {}) {

        const client = this.createGraphqlClient()
        return client.request(query, variables)
    }

    async executeCommand(variables: Object) {
        try {
            const result: any = await this.createQuery(EXECUTE_COMMAND, variables);

            const { ok, output, message } = result.executeCommand;
            return {
                ok,
                output,
                message
            };
        } catch(e) {
            console.log('Error: ', e);
            return null
        }
    }
}
