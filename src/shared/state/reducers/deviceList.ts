import {
  FETCH_DEVICE_NAMES_SUCCESS,
  FETCH_DEVICE_SERVERS_SUCCESS,
  SET_SEARCH_FILTER
} from "../actions/actionTypes";
import JiveAction from "../actions";

export interface IDeviceListState {
  nameList: string[];
  filter: string;
}

export interface IDeviceServer {
  name: string;
  server: {
    id: string;
  };
}
export interface IDeviceServerListState {
  serverNameList: IDeviceServer[];
  filter: string;
}

const initialState = {
  nameList: [],
  filter: ""
};

export function deviceList(
  state: IDeviceListState = initialState,
  action: JiveAction
): IDeviceListState {
  switch (action.type) {
    case FETCH_DEVICE_NAMES_SUCCESS: {
      const sortedDeviceNames = action.names.sort((a, b) => {
        return a.toLowerCase().localeCompare(b.toLowerCase());
      });
      return { ...state, nameList: Array.from(new Set([...state.nameList, ...sortedDeviceNames])) };
    }
    case SET_SEARCH_FILTER:
      return { ...state, filter: action.filter };
    default:
      return state;
  }
}

const initialServerState = {
  serverNameList: [],
  filter: ""
};

export function deviceServerList(
  state: IDeviceServerListState = initialServerState,
  action: JiveAction
): IDeviceServerListState {
  switch (action.type) {
    case FETCH_DEVICE_SERVERS_SUCCESS: {     
      return { ...state, serverNameList: Array.from(new Set([...state.serverNameList, ...action.devices]))};
    }
    case SET_SEARCH_FILTER:
      return { ...state, filter: action.filter };
    default:
      return state;
  }
}
