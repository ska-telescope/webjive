import attributes, { updateAttribute } from './attributes';

import {
  FETCH_DEVICE_SUCCESS,
  ATTRIBUTE_FRAME_RECEIVED,
  SET_DEVICE_ATTRIBUTE_SUCCESS,
  DEVICE_STATE_RECEIVED
} from "../actions/actionTypes";

describe('attributes', () => {
  it('should handle FETCH_DEVICE_SUCCESS action', () => {
    const state = {};
    const action = {
      type: FETCH_DEVICE_SUCCESS,
      device: {
        name: 'device1',
        attributes: [
          {
            name: 'attribute1',
            value: 'value1',
            writeValue: 'writeValue1',
            displevel: 'displevel1',
            dataformat: 'dataformat1',
            quality: 'quality1'
          }
        ]
      }
    };
    const expectedState = {
      device1: {
        attribute1: {
          name: 'attribute1',
          value: 'value1',
          writeValue: 'writeValue1',
          displevel: 'displevel1',
          dataformat: 'dataformat1',
          quality: 'quality1'
        }
      }
    };
    // @ts-ignore
    expect(attributes(state, action)).toEqual(expectedState);
  });

  it('should handle SET_DEVICE_ATTRIBUTE_SUCCESS action', () => {
    const state = {
      device1: {
        attribute1: {
          name: 'attribute1',
          value: 'value1',
          writeValue: 'writeValue1',
          displevel: 'displevel1',
          dataformat: 'dataformat1',
          quality: 'quality1'
        }
      }
    };
    const action = {
      type: SET_DEVICE_ATTRIBUTE_SUCCESS,
      attribute: {
        device: 'device1',
        name: 'attribute1',
        quality: 'newQuality',
        value: 'newValue',
        writevalue: 'newWriteValue'
      }
    };
    const expectedState = {
      device1: {
        attribute1: {
          name: 'attribute1',
          value: 'newValue',
          writeValue: 'newWriteValue',
          displevel: 'displevel1',
          dataformat: 'dataformat1',
          quality: 'newQuality'
        }
      }
    };
    // @ts-ignore
    expect(attributes(state, action)).toEqual(expectedState);
  });

  it('should handle ATTRIBUTE_FRAME_RECEIVED action', () => {
    const state = {
      device1: {
        attribute1: {
          name: 'attribute1',
          value: 'value1',
          writeValue: 'writeValue1',
          displevel: 'displevel1',
          dataformat: 'dataformat1',
          quality: 'quality1'
        }
      }
    };
    const action = {
      type: ATTRIBUTE_FRAME_RECEIVED,
      frame: {
        device: 'device1',
        attribute: 'attribute1',
        quality: 'newQuality',
        value: 'newValue',
        writeValue: 'newWriteValue'
      }
    };
    const expectedState = {
      device1: {
        attribute1: {
          name: 'attribute1',
          value: 'newValue',
          writeValue: 'newWriteValue',
          displevel: 'displevel1',
          dataformat: 'dataformat1',
          quality: 'newQuality'
        }
      }
    };
    // @ts-ignore
    expect(attributes(state, action)).toEqual(expectedState);
  });

  it('should handle DEVICE_STATE_RECEIVED action', () => {
    const state = {
      device1: {
        State: {
          name: 'State',
          value: 'oldState'
        }
      }
    };
    const action = {
      type: DEVICE_STATE_RECEIVED,
      device: 'device1',
      state: 'newState'
    };
    const expectedState = {
      device1: {
        State: {
          name: 'State',
          value: 'newState'
        }
      }
    };
    // @ts-ignore
    expect(attributes(state, action)).toEqual(expectedState);
  });

});

describe('updateAttribute', () => {
  it('should update the attribute with the given fields', () => {
    const state = {
      device1: {
        attribute1: {
          name: 'attribute1',
          value: 'value1',
          writeValue: 'writeValue1',
          displevel: 'displevel1',
          dataformat: 'dataformat1',
          quality: 'quality1'
        }
      }
    };
    const device = 'device1';
    const name = 'attribute1';
    const fields = { value: 'newValue' };
    const expectedState = {
      device1: {
        attribute1: {
          name: 'attribute1',
          value: 'newValue',
          writeValue: 'writeValue1',
          displevel: 'displevel1',
          dataformat: 'dataformat1',
          quality: 'quality1'
        }
      }
    };
    expect(updateAttribute(state, device, name, fields)).toEqual(expectedState);
  });
});