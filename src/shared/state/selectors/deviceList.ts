import { createSelector } from "reselect";
import minimatch from "minimatch";

import { IRootState } from "../reducers/rootReducer";
import { IDeviceServer } from "../reducers/deviceList";

function matchServerFilter(device: IDeviceServer, filter: string) {
  if (filter === "") {
    return true;
  }

  if (filter.match(/^glob:/)) {
    const glob = filter.replace(/^glob:/, "");
    if (device.server !== null) {
      return (
        minimatch(device.server.id, glob, { nocase: true }) ||
        minimatch(device.name, glob, { nocase: true })
      );
    } else {
      return minimatch(device.name, glob, { nocase: true });
    }
  }

  const words = filter.split(/\s+/);
  const matched = words.filter((word) => {
    if (device.server !== null) {
      return (
        device.server.id.toUpperCase().indexOf(word.toUpperCase()) !== -1 ||
        device.name.toUpperCase().indexOf(word.toUpperCase()) !== -1
      );
    } else {
      return device.name.toUpperCase().indexOf(word.toUpperCase()) !== -1;
    }
  });
  return matched.length === words.length;
}

function getDeviceListState(state: IRootState) {
  return state.deviceList;
}

export const getDeviceNames = createSelector(
  getDeviceListState,
  (state) => state.nameList
);

function getDeviceServerListState(state: IRootState) {
  return state.deviceServerList;
}

export const getDeviceServerNames = createSelector(
  getDeviceServerListState,
  (state) => state.serverNameList
);

export const getFilter = createSelector(
  getDeviceServerListState,
  (state) => state.filter
);

export const getFilteredDeviceServerNames = createSelector(
  getDeviceServerNames,
  getFilter,
  (names, filter) => {
    const filteredDevices = names.filter((name) =>
      matchServerFilter(name, filter)
    );
    return filteredDevices.map((device) => device.name);
  }
);
