import '../../tests/globalMocks';
import configureStore from "./configureStore";

describe("configureStore", () => {
  it("creates a store with the expected structure", () => {
    const store = configureStore();
    expect(store.dispatch).toBeDefined();
    expect(store.subscribe).toBeDefined();
    expect(store.getState).toBeDefined();
  });

  it("adds the websocket middleware", () => {
    const store = configureStore();
    expect(store.getState().user).toBeDefined();
    expect(store.getState().attributes).toBeDefined();
    expect(store.getState().commands).toBeDefined();
    expect(store.getState().devices).toBeDefined();
    expect(store.getState().deviceDetail).toBeDefined();
    expect(store.getState().deviceList).toBeDefined();
    expect(store.getState().database).toBeDefined();
    expect(store.getState().messages).toBeDefined();
  });

  it("adds the logger middleware in test mode", () => {
    //process.env.NODE_ENV = "test";
    const store = configureStore();
    expect(store.getState().user).toBeDefined();
    expect(store.getState().attributes).toBeDefined();
    expect(store.getState().commands).toBeDefined();
    expect(store.getState().devices).toBeDefined();
    expect(store.getState().deviceDetail).toBeDefined();
    expect(store.getState().deviceList).toBeDefined();
    expect(store.getState().database).toBeDefined();
    expect(store.getState().loggedActions).toBeDefined();
  });
});
