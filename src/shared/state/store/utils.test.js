import { retriveDeviceFromSub } from './utils'; 

describe('retriveDeviceFromSub', () => {
  test('should return an empty array when sub is undefined', () => {
    const sub = undefined;
    const result = retriveDeviceFromSub(sub);
    expect(result).toEqual([]);
  });

  test('should return the correct array of objects', () => {
    const sub = [
      'tangotest://sys/tg_test/1/ampli',
      'tangotest://sys/tg_test/1/State',
      'tangotest://sys/database/2/status',
      'tangotest://sys/database/2/State'
    ];
    const result = retriveDeviceFromSub(sub);
    expect(result).toEqual([
      {
        name: 'tangotest://sys/tg_test/1',
        attributes: [
          { name: 'ampli' },
          { name: 'State' }
        ]
      },
      {
        name: 'tangotest://sys/database/2',
        attributes: [
          { name: 'status' },
          { name: 'State' }
        ]
      }
    ]);
  });
});