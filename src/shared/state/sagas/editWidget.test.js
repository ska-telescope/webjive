import { cloneableGenerator } from "@redux-saga/testing-utils";
import { select } from "redux-saga/effects";
import { editWidget } from "./editWidget";
import {
    ADD_WIDGET,
    ADD_INNER_WIDGET,
    REORDER_INNER_WIDGET,
    DROP_INNER_WIDGET,
    MOVE_WIDGETS,
    RESIZE_WIDGET,
    DELETE_WIDGET,
    UNDO,
    REDO,
    DUPLICATE_WIDGET,
    SET_INPUT,
    DELETE_INPUT,
    ADD_INPUT,
    REORDER_WIDGETS,
    UPDATE_WIDGET,
    WIDGET_CLIPBOARD_PASTE,
    DASHBOARD_EDITED,
    SAVE_DASHBOARD,
} from "../actions/actionTypes";
import {
    getDashboards,
    getSelectedDashboard,
    getClipboardWidgets,
    getClipboardPasteCounter,
} from "../selectors";

import {
    statePasteExample,
    dashboardPasteExample,
    clipBoardExample,
    addWidgetExample,
    reorderInnerWidgetPayload,
    addInnerWidget,
    stateAddInnerWidget,
    dropInnerWidgetExample,
    moveWidgetExample,
    moveWidgetStateExample,
    resizeWidgetExample,
    deleteWidgetStateExample,
    setInputExample,
    setInputStatExample,
    addInputExample,
    addInputStateExample,
    delInputExample,
    delInputStateExample,
    reorderWidgetExample,
    reorderWidgetStateExample,
    updateWidgetExample,
    updateWidgetStateExample,
    dashboards
} from './stateTestExamples';
import { getDeviceNames } from "../selectors/deviceList";

describe("editWidget saga", () => {

    test("should handle WIDGET_CLIPBOARD_PASTE action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take WIDGET_CLIPBOARD_PASTE action
        expect(gen.next().value.payload.pattern).toContain(WIDGET_CLIPBOARD_PASTE)

        // select the state of the selected dashboard
        expect(gen.next({ type: WIDGET_CLIPBOARD_PASTE }).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(statePasteExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        expect(gen.next(dashboardPasteExample).value).toEqual(select(getClipboardWidgets));

        // select the clipBoardExample 
        expect(gen.next(clipBoardExample).value).toEqual(select(getClipboardPasteCounter));

        const editStep = gen.next(clipBoardExample.length).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle UNDO action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take UNDO action
        expect(gen.next().value.payload.pattern).toContain(UNDO)

        // select the state of the selected dashboard
        expect(gen.next({ type: UNDO }).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(statePasteExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle REDO action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take REDO action
        expect(gen.next().value.payload.pattern).toContain(REDO)

        // select the state of the selected dashboard
        expect(gen.next({ type: REDO }).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(statePasteExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle ADD_WIDGET action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take ADD_WIDGET action
        expect(gen.next().value.payload.pattern).toContain(ADD_WIDGET)

        // select the state of the selected dashboard
        expect(gen.next(addWidgetExample).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(statePasteExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle REORDER_INNER_WIDGET action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take REORDER_INNER_WIDGET action
        expect(gen.next().value.payload.pattern).toContain(REORDER_INNER_WIDGET)

        // select the state of the selected dashboard
        expect(gen.next(reorderInnerWidgetPayload).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(statePasteExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle ADD_INNER_WIDGET action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take ADD_INNER_WIDGET action
        expect(gen.next().value.payload.pattern).toContain(ADD_INNER_WIDGET)

        // select the state of the selected dashboard
        expect(gen.next(addInnerWidget).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(stateAddInnerWidget).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle DROP_INNER_WIDGET action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take DROP_INNER_WIDGET action
        expect(gen.next().value.payload.pattern).toContain(DROP_INNER_WIDGET)

        // select the state of the selected dashboard
        expect(gen.next(dropInnerWidgetExample).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(stateAddInnerWidget).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle MOVE_WIDGETS action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take MOVE_WIDGETS action
        expect(gen.next().value.payload.pattern).toContain(MOVE_WIDGETS)

        // select the state of the selected dashboard
        expect(gen.next(moveWidgetExample).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(moveWidgetStateExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });
    test("should handle RESIZE_WIDGET action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take RESIZE_WIDGET action
        expect(gen.next().value.payload.pattern).toContain(RESIZE_WIDGET)

        // select the state of the selected dashboard
        expect(gen.next(resizeWidgetExample).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(moveWidgetStateExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });
    test("should handle DUPLICATE_WIDGET action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take DUPLICATE_WIDGET action
        expect(gen.next().value.payload.pattern).toContain(DUPLICATE_WIDGET)

        // select the state of the selected dashboard
        expect(gen.next({ type: DUPLICATE_WIDGET }).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(moveWidgetStateExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle DELETE_WIDGET action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take DELETE_WIDGET action
        expect(gen.next().value.payload.pattern).toContain(DELETE_WIDGET)

        // select the state of the selected dashboard
        expect(gen.next({ type: DELETE_WIDGET }).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(deleteWidgetStateExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));

        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle SET_INPUT action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take SET_INPUT action
        expect(gen.next().value.payload.pattern).toContain(SET_INPUT)

        // select the state of the selected dashboard
        expect(gen.next(setInputExample).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(setInputStatExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));
        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle ADD_INPUT action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take ADD_INPUT action
        expect(gen.next().value.payload.pattern).toContain(ADD_INPUT)

        // select the state of the selected dashboard
        expect(gen.next(addInputExample).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(addInputStateExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));
        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle DELETE_INPUT action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take DELETE_INPUT action
        expect(gen.next().value.payload.pattern).toContain(DELETE_INPUT)

        // select the state of the selected dashboard
        expect(gen.next(delInputExample).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(delInputStateExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));
        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle REORDER_WIDGETS action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take REORDER_WIDGETS action
        expect(gen.next().value.payload.pattern).toContain(REORDER_WIDGETS)

        // select the state of the selected dashboard
        expect(gen.next(reorderWidgetExample).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(reorderWidgetStateExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));
        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);
    });

    test("should handle UPDATE_WIDGET action", () => {

        const gen = cloneableGenerator(editWidget)();
        // take UPDATE_WIDGET action
        expect(gen.next().value.payload.pattern).toContain(UPDATE_WIDGET)

        // select the state of the selected dashboard
        expect(gen.next(updateWidgetExample).value).toEqual(
            select(getSelectedDashboard)
        );

        // select the statePasteExample
        expect(gen.next(updateWidgetStateExample).value).toEqual(select(getDashboards));

        expect(gen.next().value).toEqual(select(getDeviceNames));
        // select the dashboardPasteExample
        const editStep = gen.next(dashboardPasteExample).value;
        expect(editStep.payload.action.type).toEqual(DASHBOARD_EDITED);
        expect(editStep.payload.action.dashboard.id).toEqual(statePasteExample.id);

        const finalStep = gen.next(dashboards);
        expect(finalStep.value.payload.action.type).toEqual(SAVE_DASHBOARD);

    });
});