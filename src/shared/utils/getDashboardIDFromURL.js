export function getIDFromURL() {
    const url = window.location.href;
    const idRegex = /id=([\w-]+)/;
    const match = url.match(idRegex);
    if (match && match.length > 1) {
        return match[1];
    }
    return null;
}