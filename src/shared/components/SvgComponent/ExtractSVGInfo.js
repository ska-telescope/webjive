import numeral from "numeral";
import { Parser } from 'expr-eval';

export function extractSVGInfo(svgNode) {
  const components = {};
  const rules = [];

  // Extract components with inkscape:label="rules"
  svgNode.querySelectorAll('[inkscape\\:label="rules"]').forEach((element) => {
    const descElement = element.querySelector('desc');
    if (descElement) {
      const descContent = descElement.textContent;
      const parsedRules = parseRules(descContent);
      rules.push(...parsedRules);
    }
  });

  // Extract components with <desc> containing the key 'format'
  svgNode.querySelectorAll('desc').forEach((descElement) => {
    const descContent = descElement.textContent;
    if (descContent.includes('format')) {
      const parsedDesc = parseFormat(descElement, descContent);
      Object.assign(components, parsedDesc);
    }
  });

  return { components, rules };
}

const parseRules = (descText) => {
  const rules = [];
  const lines = descText.split('\n').map(line => line.trim()).filter(line => line);

  let currentRule = {};
  lines.forEach(line => {
    if (line.startsWith('rule')) {
      if (Object.keys(currentRule).length > 0) {
        rules.push(currentRule);
      }
      currentRule = {};
    }

    // Split only at the first '=' to handle multiple '=' in the condition
    const [key, ...rest] = line.split('=').map(part => part.trim().replace(/(^"|"$)/g, ''));
    const value = rest.join('=').trim(); // Join the rest of the parts in case there are multiple '='

    if (key && value) {
      if (key === 'condition') {
        currentRule[key] = value.replace(/"/g, '');

      } else if (['model', 'type'].includes(key.trim())) {
        currentRule[key] = value.toLocaleLowerCase();

      } else {
        currentRule[key] = value;
      }
    }
  });

  if (Object.keys(currentRule).length > 0) {
    rules.push(currentRule);
  }
  return rules;
};

const parseFormat = (descElement, descText) => {
  const lines = descText.split('\n').filter(line => line.trim() !== '');
  const formatObject = {};

  if (!descElement.parentElement.id)
    descElement.parentElement.setAttribute("id", `element-${Math.floor(Math.random() * 10000)}`)

  const elementId = descElement.parentElement.id
  lines.forEach(line => {
    if (line.includes('format')) {
      const [key, value] = line.split('=').map(part => part.trim());
      if (key && value) {
        if (!formatObject[elementId]) {
          formatObject[elementId] = { [key]: [] };
        }

        formatObject[elementId][key].push(value);
      }
    }
  });

  return formatObject;
};

/**
 * This returns the layers which are used in rules.
 * These rules show/hide dynamically during run
 */
export const getConditionalLayers = (rules) => {
  const conditionalLayers = []
  if (rules) {
    rules?.forEach(rule => {
      if (rule?.type?.toLowerCase() === "layer" && rule?.condition) {
        conditionalLayers.push(rule.layer);
      }
    })
  }
  return conditionalLayers;
}

function preprocessExpression(exp) {
  const operators = new Set(['and', 'or', 'not', 'in']);
  const constants = new Set(['true', 'false', 'null', 'undefined']);
  const variables = new Set(['value']);

  // Add quotes around any unquoted words (like DEGRADED) in the expression
  return exp.replace(/([a-zA-Z_]\w*)/g, (match) => {
    // If it's not a known variable, operator, or constant, wrap it in quotes
    if (!operators.has(match) && !constants.has(match) && !variables.has(match)) {
      return `"${match}"`;
    }
    return match;
  });
}

export function stringToBoolean(str) {
  if (typeof str === 'string') {
    return str.toLowerCase() === 'true';
  }
  return false;
}

export function evaluateExpression(exp, varr) {
  const parser = new Parser();
  // Preprocess expression to handle string literals
  const processedExp = preprocessExpression(exp);

  // Ensure values in varr are cast to the appropriate type (number or string)
  const formattedVarr = Object.fromEntries(
    Object.entries(varr).map(([key, value]) => {
      // Try to convert to number if possible, otherwise keep as string
      const parsedValue = typeof value === 'boolean' ? value
        : (typeof value === 'string' && (value === 'true' || value === 'false')) ? stringToBoolean(value)
        : isNaN(value) ? value
        : parseFloat(value)
      return [key, parsedValue];
    })
  );

  try {
    return parser.parse(processedExp).evaluate(formattedVarr);
  } catch (error) {
    console.error('Expression evaluation error:', error);
    return false; // Return false if the expression cannot be evaluated
  }
}

const applyRules = (extractions, item) => {
  extractions?.rules?.forEach((rule) => {
    try {
      const attr = `${item.deviceName}/${item.attributeName}`;
      // Filter css rule and apply conditional style
      if (
        -1 !== rule.model.indexOf(attr) &&
        rule?.type === 'css'
      ) {
        const newCss = evaluateExpression(rule.condition, {
          value: item.lastValue,
        })
          ? rule.ifcss
          : rule.elsecss;

        const styleObj = JSON.parse(newCss);
        const element = document.querySelector(
          '[data-model$="' + attr + '"]'
        );
        if (element) {
          Object.assign(element['style'], styleObj);
        }
      }
    } catch (err) {
      console.log('Rule Error: ', err);
    }
  });
};

export const getFormattedValue = (extractions, item) => {
  applyRules(extractions, item);
  let formattedValue = item.lastValue;
  const formats = extractions?.components[item.selector]?.format;

  const applyIndexFormat = (format) => {
    const index = parseInt(format.substring(format.indexOf('(') + 1, format.indexOf(')')).trim());
    const splitArray = formattedValue.split(',');
    formattedValue = splitArray[index].trim();
  };

  const applyConcatFormat = (format) => {
    const concatStr = format.substring(
      format.indexOf('(') + 1,
      format.indexOf(')')
    );
    formattedValue = concatStr + formattedValue;
  };

  const applySubstrFormat = (format) => {
    const params = format
      .substring(format.indexOf('(') + 1, format.indexOf(')'))
      .split(',');
    const startIndex = parseInt(params[0].trim());
    const endIndex = parseInt(params[1].trim());
    const resultArray = formattedValue.split(',').slice(startIndex, endIndex);
    formattedValue = resultArray.join(',');
  };

  const applyNumeralFormat = (format) => {
    const formatRule = format.substring(
      format.indexOf('(') + 1,
      format.indexOf(')')
    );

    formattedValue = numeral(formattedValue).format(formatRule);
  };

  const applyJsonPathFormat = (format) => {
    const path = format.trim().split('/').filter(r => r);
    let value = JSON.parse(formattedValue);
    for (let i = 0; i < path.length; i++) {
      value = value[path[i]];
    }
    formattedValue = JSON.stringify(value);
  };

  formats?.forEach((format) => {
    try {
      if (format.includes('index')) {
        applyIndexFormat(format);
      } else if (format.includes('concat')) {
        applyConcatFormat(format);
      } else if (format.includes('substr')) {
        applySubstrFormat(format);
      } else if (format.startsWith('/')) {
        applyJsonPathFormat(format);
      } else if (format.includes('numeral')) {
        applyNumeralFormat(format);
      }
    } catch (err) {
      console.log(`Svg formatting error for format: ${format} - ${err}`);
    }
  });

  return formattedValue;
};
