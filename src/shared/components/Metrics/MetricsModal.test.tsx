import React from 'react';
import { render, screen } from '@testing-library/react';
import MetricsModal from './MetricsModal';
import { Provider } from "react-redux";
import configureStore from 'redux-mock-store';

const tangoDB = "testdb"

const mockStore = configureStore([]);
const mockState = {
  communicationHealth: {
    metrics: {
      executionTime: {
        query: [
          { argument: 'query1', duration: 123 },
          { argument: 'query2', duration: 456 },
        ],
        mutation: [
          { argument: 'mutation1', duration: 789 },
        ],
      },
      subscribedAttrs: [
        { attribute: 'testdb://sys/tg_test/1/double_scalar', eventType: 'type1', listeners: 2, name: 'sys/tg_test/1', deviceAccessible: true },
        { attribute: 'attr2', eventType: 'type2', listeners: 1, name: 'device2', deviceAccessible: false },
      ],
    },
  },
  attributes: {
    [`${tangoDB}://sys/tg_test/1/double_scalar`]: { double_scalar: {name: "sys/tg_test/1", value: "1", writeValue: false, displevel: "INFO", dataformat: "", quality: ""} },
  },
  database: { tangoDBName: tangoDB, info: "" },
  selectedDashboard: {
    "widgets": {
        "1": {
            "_id": "664472b0913dd400120732d8",
            "id": "1",
            "x": 38,
            "y": 14,
            "canvas": "0",
            "width": 10,
            "height": 2,
            "type": "ATTRIBUTE_DISPLAY",
            "inputs": {
                "attribute": {
                    "device": "testdb://sys/tg_test/1",
                    "attribute": "double_scalar",
                    "label": "double_scalar"
                },
                "precision": 2,
                "format": "",
                "showUnit": true,
                "showDevice": false,
                "jsonCollapsed": true,
                "showTangoDB": false,
                "showAttribute": "Label",
                "alignTextCenter": false,
                "alignValueRight": true,
                "scientificNotation": false,
                "showEnumLabels": true,
                "showAttrQuality": false,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "font": "Helvetica",
                "widgetCss": ""
            },
            "order": 0,
            "valid": 1,
            "disabled": false
        }
    },
    "id": "664472a5913dd400120732d4",
}
};

const store = mockStore(mockState);

describe('MetricsModal component', () => {
  it('renders without crashing', async () => {
    render(<Provider store={store}><MetricsModal setState={() => {}} /></Provider>);
    
    await screen.findByText('TangoGQL Health Status & Communication Metrics');

    expect(screen.getByText('TangoGQL Health Status & Communication Metrics').innerHTML).toContain("TangoGQL Health Status &amp; Communication Metrics")

  });

  it('renders received data correctly', async () => {
    render(<Provider store={store}><MetricsModal setState={() => {}} /></Provider>);
    
    await screen.findByText('Data received');
    await screen.queryByText('sys/tg_test/1/double_scalar/double_scalar');
  
    const element = await screen.queryByText('sys/tg_test/1/double_scalar/double_scalar');

    expect(element).toBeDefined(); 
  });

  it('renders metrics', async () => {
    render(<Provider store={store}><MetricsModal setState={() => {}} /></Provider>);

    await screen.findByText('Metrics');
    await screen.findByText('query1');
    await screen.findByText('123');

    const element1 = await screen.findByText('query1');
    const element2 = await screen.findByText('123');

    expect(element1).toBeDefined();
    expect(element2).toBeDefined();
  });

});
