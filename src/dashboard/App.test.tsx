// App.test.tsx
import React from 'react';
import { render, screen } from '@testing-library/react';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import App from './App';

// Mock child components
jest.mock('../shared/user/components/UserAware', () => ({ children }: any) => <div data-testid="UserAware">{children}</div>);
jest.mock('./components/Dashboard', () => () => <div data-testid="Dashboard">Dashboard Component</div>);

// Mock external dependencies
const mockStore = configureStore([]);
let store: MockStoreEnhanced<unknown, {}>;
const mockDispatch = jest.fn();

beforeEach(() => {
  store = mockStore({
    // Define initial state slices as needed
    user: {
      isAuthenticated: true,
      // ...other user state
    },
    // Add other necessary initial state slices here
  });

  store.dispatch = mockDispatch;

  // Mock window.config
  (window as any).config = {
    basename: '/app',
  };
});

afterEach(() => {
  jest.clearAllMocks();
});

// Utility function to render components with necessary providers
const renderWithProviders = (ui: React.ReactElement, history: any) => {
  return render(
    <Provider store={store}>
      <Router history={history}>
        {ui}
      </Router>
    </Provider>
  );
};

describe('App Component', () => {
  it('renders UserAware component', () => {
    const history = createMemoryHistory();
    renderWithProviders(<App />, history);

    expect(screen.getByTestId('UserAware')).not.toBeNull();
  });

});
