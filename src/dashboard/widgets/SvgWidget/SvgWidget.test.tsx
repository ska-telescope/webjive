import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import {
  FileInputDefinition
} from "../../types";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";
import SVGWidget from "./SvgWidget";

jest.mock("../../../shared/components/SvgComponent/SvgComponent", () => () => <div>Mock SVG Component</div>);

configure({ adapter: new Adapter() });
const store = configureStore();
let myFile: FileInputDefinition = {type: "file", label: "SVG file", fileAccepted: ".svg"};

describe("Svg widget", () => {
  it("renders widget correctly", () => {
    let element = React.createElement(SVGWidget.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 25,
      actualHeight: 25,
      inputs: {
        title: "",
        svgFile: myFile,
        svgCss: "",
        svgUploaded: "",
        overflow: false,
      },
    });

    const wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.html()).toContain("svg-wrapper");
  });

  it("renders widget with title", () => {
    let element = React.createElement(SVGWidget.component, {
      mode: "run",
      t0: 1,
      id: 42,
      actualWidth: 25,
      actualHeight: 25,
      inputs: {
        title: "My Title",
        svgFile: myFile,
        svgCss: "",
        svgUploaded: "",
        overflow: true
      },
    });

    const wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.html()).toContain("My Title");
    expect(wrapper.html()).toContain("Mock SVG Component");
  });
});
