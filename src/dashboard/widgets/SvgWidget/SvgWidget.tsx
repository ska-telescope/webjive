import React from "react";
import { WidgetProps } from "../types";
import {
  WidgetDefinition,
  FileInputDefinition,
  StyleInputDefinition,
  StringInputDefinition,
  BooleanInputDefinition,
} from "../../types";
import SvgComponent from "../../../shared/components/SvgComponent/SvgComponent";
import sampleSvg from "./sampleSvg";

type Inputs = {
  svgFile: FileInputDefinition | any;
  svgUploaded: StringInputDefinition;
  title: StringInputDefinition;
  svgCss: StyleInputDefinition;
  overflow: BooleanInputDefinition;
};

type Props = WidgetProps<Inputs>;

const SvgWidget: React.FC<Props> = (props) => {
  const { mode, inputs } = props;
  let { svgFile, title } = inputs;

  svgFile = svgFile || "{}";
  let svgFileContent = sampleSvg;
  if (mode !== "library") {
    const svgFileString =
      typeof svgFile === "string"
        ? svgFile
        : JSON.stringify({ fileContent: svgFile });
    try {
      const parsedFile = JSON.parse(svgFileString);
      svgFileContent = parsedFile.fileContent || "{}";
    } catch (error) {
      console.error("Error parsing svgFile:", error);
      svgFileContent = sampleSvg;
    }
  }

  return (
    <div className="svg-wrapper">
      {title !== "" && <div className="text-center">{title}</div>}
      <SvgComponent
        path={svgFileContent}
        mode={mode}
        zoom={false}
        layers={false}
        svgCss={props.inputs.svgCss}
        viewportSvgDimensions={{ width: 0, height: 0 }}
      />
    </div>
  );
};

const definition: WidgetDefinition<Inputs> = {
  type: "SVG_WIDGET",
  name: "SVG Widget",
  defaultWidth: 10,
  defaultHeight: 10,
  inputs: {
    title: {
      type: "string",
      label: "Title",
    },
    svgFile: {
      type: "file",
      label: "SVG File",
      fileAccepted: ".svg",
    },
    svgUploaded: {
      type: "string",
      label: "SVG Uploaded",
      default: "None",
      dependsOn: "svgFile",
    },
    svgCss: {
      type: "style",
      default: "",
      label: "SVG Css",
      skipValidation: true
    },
    overflow: {
      type: "boolean",
      default: false,
      label: "Show overflow scroll",
    }
  },
};

const SVGWidget = { component: SvgWidget, definition };
export default SVGWidget;
