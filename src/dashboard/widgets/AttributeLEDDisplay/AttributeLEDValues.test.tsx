import React from "react";
import { render } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import AttributeLEDValues from "./AttributeLEDValues";
import {
    getAttributeLastTimeStampFromState as getAttributeLastTimeStampFromStateMock,
    getAttributeLastValueFromState as getAttributeLastValueFromStateMock,
  } from "../../../shared/utils/getLastValueHelper";

  jest.mock("react-redux", () => ({
    useSelector: jest.fn()
  }));
  
const getAttributeLastTimeStampFromState = getAttributeLastTimeStampFromStateMock as jest.Mock;
const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
    getAttributeLastValueFromState: jest.fn(),
    getAttributeLastTimeStampFromState: jest.fn(),
  }));

const useSelector = useSelectorMock as jest.Mock;

describe("AttributeLEDValues", () => {

    let deviceName='sys/tg_test/1'
    let attributeName='double_scalar'
    let trueColor =  "#00ff00"  
    let falseColor = "#ff0000"  
    let emledSize = "1em"  
    let minMargin = "0em 0.5em"  
    let textSize =  1  
    let compare =  "200"  
    let relation = ">"  
    let showAttributeValue = true


    beforeEach(() => {
        useSelector.mockImplementation((selectorFn: any) =>
          selectorFn({
                messages: {
                    'sys/tg_test/1': {
                    attributes: {
                        double_scalar: {
                        values: [
                            201.73105033133845,
                            201.73105033133845,
                            198.94967251428307
                        ],
                        quality: [
                            'ATTR_VALID',
                            'ATTR_VALID',
                            'ATTR_VALID'
                        ],
                        timestamp: [
                            1689758967.68244,
                            1689758967.71221,
                            1689758970.719745
                        ]
                        }
                    }
                    }
                },
                ui: {
                  mode: "run",
                },
              })
        );
        getAttributeLastValueFromState.mockReturnValue(201);
        getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
      });

      afterEach(() => {
        jest.resetAllMocks();
      });

      it("should render the attrute value value from store", () => {
        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={compare}
                relation={relation}
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).toContain('201');
      });

      it("hides the attribute name if showAttribute is not set", () => {
        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={compare}
                relation={relation}
                showAttributeValue={false}
            />
        );
    
        expect(container.innerHTML).not.toContain('201');
      });

      it("shows true color when match the relation", () => {
        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={compare}
                relation={relation}
                showAttributeValue={showAttributeValue}
            />
        );

        expect(container.innerHTML).toContain('rgb(0, 255, 0)'); //green
      });

      it("shows false color when it doesn't match the relation", () => {
        
        getAttributeLastValueFromState.mockReturnValue(199);

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={compare}
                relation={relation}
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).toContain('rgb(255, 0, 0)'); //red
      });

      it("shows ... when the value is undefined", () => {
        
        getAttributeLastValueFromState.mockReturnValue(undefined);

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={compare}
                relation={relation}
                showAttributeValue={showAttributeValue}
            />
        );    
        expect(container.innerHTML).toContain('...'); 
      });

      it("compares <", () => {
        
        getAttributeLastValueFromState.mockReturnValue(199);

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={compare}
                relation="<"
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).toContain('rgb(0, 255, 0)'); 
      });

      it("compares <=", () => {
        
        getAttributeLastValueFromState.mockReturnValue(200);

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={compare}
                relation="<="
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).toContain('rgb(0, 255, 0)'); 
      });

      it("compares >=", () => {
        
        getAttributeLastValueFromState.mockReturnValue(200);

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={compare}
                relation=">="
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).not.toContain('rgb(255, 0, 0)'); 
      });

      it("compares =", () => {
        
        getAttributeLastValueFromState.mockReturnValue(200);

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={compare}
                relation="="
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).toContain('rgb(0, 255, 0)'); 
      });

      it("compares default", () => {
        
        getAttributeLastValueFromState.mockReturnValue(200);

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={compare}
                relation="default"
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).toContain('rgb(255, 0, 0)'); 
      });

      it("compares string", () => {
        
        getAttributeLastValueFromState.mockReturnValue("default Attribute");

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={"default Attribute"}
                relation="="
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).toContain('rgb(0, 255, 0)'); 
      });

      it("compares string without =", () => {
        
        getAttributeLastValueFromState.mockReturnValue("default Attribute");

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={"default Attribute"}
                relation="<"
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).toContain('background-color: rgb(255, 255, 255)'); 
      });

      it("compares true boolean", () => {
        
        getAttributeLastValueFromState.mockReturnValue(true);

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={"true"}
                relation="="
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).toContain('rgb(0, 255, 0)'); 
        expect(container.innerHTML).toContain('true'); 
      });

      it("compares false boolean", () => {
        
        getAttributeLastValueFromState.mockReturnValue(false);

        const { container } = render(
            <AttributeLEDValues 
                deviceName={deviceName}
                attributeName={attributeName}
                trueColor={trueColor}
                falseColor={falseColor}
                emledSize={emledSize}
                minMargin={minMargin}
                textSize={textSize}
                compare={"true"}
                relation="="
                showAttributeValue={showAttributeValue}
            />
        );
    
        expect(container.innerHTML).toContain('rgb(255, 0, 0)'); 
        expect(container.innerHTML).toContain('false'); 
      });

});