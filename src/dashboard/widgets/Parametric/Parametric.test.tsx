import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";
import { ParametricWidget } from "./Parametric";
import { Dashboard, DashboardEditHistory } from "../../types";
import "@testing-library/jest-dom";

const store = configureStore();
const saveDashboard = jest.fn();
const changeSubscription = jest.fn();
const widgets = [
  {
    _id: "607e8e9c99ba3e0017cf9080",
    id: "1",
    x: 16,
    y: 5,
    canvas: "0",
    width: 14,
    height: 3,
    type: "ATTRIBUTE_DISPLAY",
    inputs: {
      attribute: {
        device: "CSPVar1",
        attribute: "state",
        label: "State",
      },
      precision: 2,
      showDevice: false,
      showAttribute: "Label",
      scientificNotation: false,
      showEnumLabels: false,
      textColor: "#000000",
      backgroundColor: "#ffffff",
      size: 1,
      font: "Helvetica",
    },
    order: 0,
    valid: 1,
  },
  {
    _id: "607e946599ba3e0017cf90a0",
    id: "2",
    x: 16,
    y: 10,
    canvas: "0",
    width: 20,
    height: 3,
    type: "COMMAND_ARRAY",
    inputs: {
      command: {
        device: "myvar",
        command: "DevString",
        acceptedType: "DevString",
      },
      showDevice: true,
      showCommand: true,
      requireConfirmation: true,
      displayOutput: true,
      OutputMaxSize: 20,
      timeDisplayOutput: 3000,
      textColor: "#000000",
      backgroundColor: "#ffffff",
      size: 1,
      font: "Helvetica",
    },
    order: 1,
    valid: 1,
  },
];
const myHistory: DashboardEditHistory = {
  undoActions: [{ "1": widgets[0] }],
  redoActions: [{ "1": widgets[0] }],
  undoIndex: 1,
  redoIndex: 2,
  undoLength: 3,
  redoLength: 4,
};
const selectedDashboard: any = {
  selectedId: null,
  selectedIds: ["1"],
  widgets: widgets,
  id: "604b5fac8755940011efeea1",
  name: "mydashboard",
  user: "user1",
  group: null,
  groupWriteAccess: false,
  lastUpdatedBy: "user1",
  insertTime: new Date("2021-04-11T15:27:03.803Z"),
  variables: [],
};

const dashboards: Dashboard[] = [
  {
    id: "604b5fac8755940011efeea1",
    name: "Untitled dashboard",
    user: "user1",
    groupWriteAccess: true,
    selectedIds: ["aaa"],
    history: myHistory,
    insertTime: new Date("2021-03-12T12:33:48.981Z"),
    updateTime: new Date("2021-04-20T08:12:53.689Z"),
    group: null,
    lastUpdatedBy: "user1",
    environment: [],
    variables: [
      {
        _id: "0698hln1j359a",
        name: "myvar",
        class: "tg_test",
        device: "sys/tg_test/1",
      },
    ],
  },
  {
    id: "6073154703c08b0018111066",
    name: "Untitled dashboard",
    user: "user1",
    groupWriteAccess: true,
    selectedIds: ["aaa"],
    history: myHistory,
    insertTime: new Date("2021-04-11T15:27:03.803Z"),
    updateTime: new Date("2021-04-20T08:44:21.129Z"),
    group: "aGroup",
    lastUpdatedBy: "user1",
    environment: [],
    variables: [
      {
        _id: "78l4d190kh2g",
        name: "cspvar1",
        class: "tg_test",
        device: "sys/tg_test/1",
      },
      {
        _id: "i5am90g1il0e",
        name: "myvar",
        class: "tg_test",
        device: "sys/tg_test/1",
      },
    ],
  },
];

const renderComponent = ({
  selectedDashboardOverride = selectedDashboard,
  dashboardsOverride = dashboards,
} = {}) => {
  return render(
    <Provider store={store}>
      <ParametricWidget.WrappedComponent
        mode="run"
        inputs={{
          name: {
            type: "string",
            label: "Name:",
            default: "Name",
            placeholder: "Name of variable",
            required: true,
          },
          variable: {
            type: "variable",
            label: "Variable:",
          },
        }}
        selectedDashboard={selectedDashboardOverride}
        dashboards={dashboardsOverride}
        tangoDB="testdb"
        saveDashboard={jest.fn()}
        changeSubscription={jest.fn()}
        updateState={() => {}}
      />
    </Provider>
  );
};

describe("Parametric Widget", () => {
  let changeDevice = jest.fn();
  const selectedDashboard: any = {
    selectedId: null,
    selectedIds: ["1"],
    widgets: {
      "1": {
        id: "1",
        x: 19,
        y: 8,
        canvas: "0",
        width: 10,
        height: 2,
        type: "ATTRIBUTE_DISPLAY",
        inputs: {
          attribute: {
            device: "sys/tg_test/1",
            attribute: "state",
            label: "State",
          },
          precision: 2,
          showDevice: false,
          showAttribute: "Label",
          scientificNotation: false,
          showEnumLabels: false,
        },
        valid: 1,
        order: 0,
      },
    },
    id: "604b5fac8755940011efeea1",
    name: "mydashboard",
    user: "user1",
    group: null,
    groupWriteAccess: false,
    lastUpdatedBy: "user1",
    insertTime: new Date("2021-04-11T15:27:03.803Z"),
    variables: [],
  };
  const widgets = [
    {
      _id: "607e8e9c99ba3e0017cf9080",
      id: "1",
      x: 16,
      y: 5,
      canvas: "0",
      width: 14,
      height: 3,
      type: "ATTRIBUTE_DISPLAY",
      inputs: {
        attribute: {
          device: "CSPVar1",
          attribute: "state",
          label: "State",
        },
        precision: 2,
        showDevice: false,
        showAttribute: "Label",
        scientificNotation: false,
        showEnumLabels: false,
        textColor: "#000000",
        backgroundColor: "#ffffff",
        size: 1,
        font: "Helvetica",
      },
      order: 0,
      valid: 1,
    },
    {
      _id: "607e946599ba3e0017cf90a0",
      id: "2",
      x: 16,
      y: 10,
      canvas: "0",
      width: 20,
      height: 3,
      type: "COMMAND_ARRAY",
      inputs: {
        command: {
          device: "myvar",
          command: "DevString",
          acceptedType: "DevString",
        },
        showDevice: true,
        showCommand: true,
        requireConfirmation: true,
        displayOutput: true,
        OutputMaxSize: 20,
        timeDisplayOutput: 3000,
        textColor: "#000000",
        backgroundColor: "#ffffff",
        size: 1,
        font: "Helvetica",
      },
      order: 1,
      valid: 1,
    },
  ];
  let myHistory: DashboardEditHistory;
  myHistory = {
    undoActions: [{ "1": widgets[0] }],
    redoActions: [{ "1": widgets[0] }],
    undoIndex: 1,
    redoIndex: 2,
    undoLength: 3,
    redoLength: 4,
  };
  const dashboards: Dashboard[] = [
    {
      id: "604b5fac8755940011efeea1",
      name: "Untitled dashboard",
      user: "user1",
      environment: [],
      groupWriteAccess: true,
      selectedIds: ["aaa"],
      history: myHistory,
      insertTime: new Date("2021-03-12T12:33:48.981Z"),
      updateTime: new Date("2021-04-20T08:12:53.689Z"),
      group: null,
      lastUpdatedBy: "user1",
      variables: [
        {
          _id: "0698hln1j359a",
          name: "myvar",
          class: "tg_test",
          device: "sys/tg_test/1",
        },
      ],
    },
    {
      id: "6073154703c08b0018111066",
      name: "Untitled dashboard",
      user: "user1",
      environment: [],
      groupWriteAccess: true,
      selectedIds: ["aaa"],
      history: myHistory,
      insertTime: new Date("2021-04-11T15:27:03.803Z"),
      updateTime: new Date("2021-04-20T08:44:21.129Z"),
      group: "aGroup",
      lastUpdatedBy: "user1",
      variables: [
        {
          _id: "78l4d190kh2g",
          name: "cspvar1",
          class: "tg_test",
          device: "sys/tg_test/1",
        },
        {
          _id: "i5am90g1il0e",
          name: "myvar",
          class: "tg_test",
          device: "sys/tg_test/1",
        },
      ],
    },
  ];
  const tangoClasses = [
    { name: "DataBase", devices: [{ name: "sys/database/2" }] },
    {
      name: "DServer",
      devices: [
        { name: "dserver/DataBaseds/2" },
        { name: "dserver/Starter/f5d56d5f249f" },
        { name: "dserver/TangoAccessControl/1" },
        { name: "dserver/TangoTest/test" },
        { name: "dserver/tarantaTestDevice/test" },
      ],
    },
    { name: "Starter", devices: [{ name: "tango/admin/f5d56d5f249f" }] },
    { name: "TangoAccessControl", devices: [{ name: "sys/access_control/1" }] },
    { name: "TangoTest", devices: [{ name: "sys/tg_test/1" }] },
    {
      name: "tarantaTestDevice",
      devices: [{ name: "test/tarantatestdevice/1" }],
    },
  ];

  it("renders without crashing", () => {
    const props = {
      username: "CREAM",
      selectedDashboardOverride: selectedDashboard,
      dashboards: dashboards,
      changeDevice: changeDevice(),
      fetchClasses: () => {
        return tangoClasses;
      },
    };

    renderComponent(props);
    expect(screen.getByText("No device found")).toBeInTheDocument();
    fireEvent.change(screen.getByRole("combobox"), {
      target: { value: "sys/tg_test/1" },
    });
    expect(changeDevice).toHaveBeenCalled();
  });

  it("renders without name variable", () => {
    const { container } = renderComponent();
    const element = container.querySelector(
      ".parametric-dropdown"
    ) as HTMLSelectElement;
    expect(element).not.toBeDisabled();

    fireEvent.change(element, {
      target: { value: "dserver/Starter/f5d56d5f249f" },
    });

    expect(screen.getByText("No device found")).toBeInTheDocument();

    waitFor(() => {
      expect(saveDashboard).toHaveBeenCalledTimes(1);
      expect(changeSubscription).toHaveBeenCalledTimes(1);
    });

    waitFor(() => {
      expect(element.value).toBe("dserver/Starter/f5d56d5f249f");
    });
  });

  it("updates select options when devices are fetched", () => {
    jest.mock("../../../shared/api/tangoAPI", () => ({
      fetchClassAndDevices: jest.fn().mockResolvedValue([
        {
          devices: [
            { name: "sys/tg_test/1" },
            { name: "dserver/Starter/f5d56d5f249f" },
          ],
        },
      ]),
    }));

    const variables = [
      {
        name: "myvar",
        class: "TangoTest",
        device: "sys/tg_test/1",
      },
    ] as any;

    renderComponent({
      selectedDashboardOverride: {
        ...selectedDashboard,
        variables: variables,
      },
    });

    waitFor(() => {
      expect(
        screen.getByRole("option", { name: "sys/tg_test/1" })
      ).toBeInTheDocument();
      expect(
        screen.getByRole("option", { name: "dserver/Starter/f5d56d5f249f" })
      ).toBeInTheDocument();
    });
  });
});
