const widgetTypes = {
    EDIT_WIDGET: "EDIT_WIDGET",
    LIBRARY_WIDGET: "LIBRARY_WIDGET",
};

export default widgetTypes;