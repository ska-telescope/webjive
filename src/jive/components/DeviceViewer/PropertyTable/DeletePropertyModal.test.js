import React from "react";
import { render, fireEvent } from "@testing-library/react";
import DeletePropertyModal from "./DeletePropertyModal";

test("DeletePropertyModal component", () => {
  const name = "property name";
  const onDelete = jest.fn();
  const onClose = jest.fn();

  const { getByText } = render(
    <DeletePropertyModal name={name} onDelete={onDelete} onClose={onClose} />
  );

  const deleteButton = getByText("Delete");
  const cancelButton = getByText("Cancel");

  // check if onDelete is called when delete button is clicked
  fireEvent.click(deleteButton);
  expect(onDelete).toHaveBeenCalledWith(name);

  // check if onClose is called when cancel button is clicked
  fireEvent.click(cancelButton);
  expect(onClose).toHaveBeenCalled();
});
