import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import DeviceList from './DeviceList';
import { setDeviceFilter } from '../../../shared/state/actions/deviceList';

import { BrowserRouter } from "react-router-dom";

jest.mock('../../../shared/state/actions/deviceList', () => ({
  setDeviceFilter: jest.fn(),
}));
jest.mock('../../../shared/state/actions/tango', () => ({
  getDeviceServers: jest.fn(),
}));
jest.mock('query-string', () => ({
  parse: jest.fn(),
}));

const testDeviceNames = [
  'sys/tg_test/1',
  'sys/tg_test/2',
  'sys/tg_test/3',
  'sys/tg_test/4',
];

const testFilter = 'test';

let loading = false;
let initalState = {
  deviceServerList: {
    serverNameList: [
      {
        name: "dserver/databaseds/2",
        server:{id:"DataBaseds"}
      },
      {
        name: "dserver/starter/8507b18b52c9",
        server:{id:"DataBaseds"}
      },
      {
        name: "dserver/tangoaccesscontrol/1",
        server:{id:"DataBaseds"}
      },
      {
        name: "dserver/tangotest/test",
        server:{id:"TangoTest"}
      },
      {
        name: "sys/tg_test/1",
        server:{id:"TangoTest"}
      },
  ], 
    filter: ''
  },
  loadingStatus: {
      loadingNames: false,
      loadingDevice: false,
      loadingOutput: {}
  }
};

describe('DeviceList', () => {
  it('renders correctly', () => {
    const props = {};
    const { container } = render(
      <Provider store={createStore(() => (initalState))}>
        <BrowserRouter>
        <DeviceList
          tangoDB="test"
          deviceNames={testDeviceNames}
          currentDeviceName={testDeviceNames[0]}
          filter={testFilter}
          loading={loading}
          onSetFilter={setDeviceFilter}
          location={{ search: '' }}
          {...props}
        />
        </BrowserRouter>
      </Provider>
    );

    expect(container.innerHTML).not.toContain('loading');
  });

  it('renders correctly with loading true', () => {

    const { container } = render(
      <Provider store={createStore(() => ({
        deviceServerList: {
          serverNameList: [
            {
              name: "dserver/databaseds/2",
              server:{id:"DataBaseds"}
            },
            {
              name: "dserver/starter/8507b18b52c9",
              server:{id:"DataBaseds"}
            },
            {
              name: "dserver/tangoaccesscontrol/1",
              server:{id:"DataBaseds"}
            },
            {
              name: "dserver/tangotest/test",
              server:{id:"TangoTest"}
            },
            {
              name: "sys/tg_test/1",
              server:{id:"TangoTest"}
            },
          ], 
          filter: ''
        },
        loadingStatus: {
            loadingNames: true,
            loadingDevice: true,
            loadingOutput: {}
        }
      }))}>
        <BrowserRouter>
        <DeviceList
          tangoDB="test"
          deviceNames={testDeviceNames}
          currentDeviceName={testDeviceNames[0]}
          filter={testFilter}
          loading={true}
          onSetFilter={setDeviceFilter}
          location={{ search: '' }}
        />
        </BrowserRouter>
      </Provider>
    );

    expect(container.innerHTML).toContain('loading');
  });

  it('renders correctly with deviceName null', () => {

    const { container } = render(
      <Provider store={createStore(() => ({
        deviceServerList: {
          serverNameList: [
            {
              name: "dserver/databaseds/2",
              server:{id:"DataBaseds"}
            },
            {
              name: "dserver/starter/8507b18b52c9",
              server:{id:"DataBaseds"}
            },
            {
              name: "dserver/tangoaccesscontrol/1",
              server:{id:"DataBaseds"}
            },
            {
              name: "dserver/tangotest/test",
              server:{id:"TangoTest"}
            },
            {
              name: "sys/tg_test/1",
              server:{id:"TangoTest"}
            },
          ], 
          filter: ''
        },
        loadingStatus: {
            loadingNames: true,
            loadingDevice: true,
            loadingOutput: {}
        }
      }))}>
        <BrowserRouter>
        <DeviceList
          tangoDB="test"
          deviceNames={testDeviceNames}
          currentDeviceName={null}
          filter={testFilter}
          loading={true}
          onSetFilter={setDeviceFilter}
          location={{ search: '' }}
        />
        </BrowserRouter>
      </Provider>
    );

    expect(container.innerHTML).toContain('loading');
  });

});