How to install Taranta
**********************


Required systems and technologies
===================================

The developer need to have installations of:

    - Make              (4.1 or above)
    - Git               (2.17.1 or above)
    - Docker            (19.03.13 or above)
    - Docker-compose    (1.23.2 or above)

The supported OS is ubuntu 18.04

Current version of Taranta
=============================

The current version of Taranta is |release|.

The installation project for Taranta can be found at:
https://gitlab.com/tango-controls/web/taranta-suite
Please read the README.md there for more installation instructions


How to run Taranta
==================

Taranta one command setup
Intended to facilitate local development. Subject to change.

Requirements

    - Make
    - git
    - Docker
    - docker-compose

Usage

Execute

.. code-block:: bash

 make run

and go to http://localhost:22484/testdb in your web browser.