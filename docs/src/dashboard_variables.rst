Configuring Parametric Dashboard
*********************************


Overview
===================

From the 1.1.0 Taranta version, users are able to build parametric dashboards using dashboard variables. 

A parametric dashboard is a dashboard where the widgets can be configured using a variable defined by the user, instead of a running device. 

A dashboard variable is composed of: 
- Name: defined by the user, it is unique and unchangeable  
- Tango Class: represents the Class associate with the variable
- Default Device: it is the device running as default when the user starts the dashboard

During a widget configuration in edit mode, the user can select a variable instead of a running device. Then, the inspector will retrieve the attributes and the commands from the default device. 

At runtime, the user can change the device associated with the variable selecting one of the device belonging to the Tango Class defined in the variable. It will affect all the widgets associated with the variable. 

A dashboard can contain 0 or more than 1 variable. 

It is possible to export/import a dashboard with  variables. Taranta performs checks in order to guarantee that the variables defined in the dashboard are consistent with the running devices. 
 
How to use dashboard variables
=================================
When running such a parametric dashboard, the device variables get the value of the default device. The user is able to see and access the 'Variable Selector' widget, from which to select another device of the class, and the dashboard is re-rendered with attributes and states deriving from that device.

If there are 2+ variables, it is possible to use 2+ ``Variable Selector`` widget, one for each variable. If a ``Variable Selector`` widget is not used in the dashboard, the dashboard will run with the default device associated and the user will not able to change it. 

Variables can be configured from below icon next to ``export dashboard`` icon

\ |IMG1|\ 


Creating a variable
---------------------

New variable can be created by clicking ``Add new variable`` button on the modal. After filling all details ``Add Variables`` will be enabled

Edit a variable
---------------------

Variable name, device class & device name can be updated clicking on the ``pencil` icon in the variable row. User needs to click ``Save`` button to persist changes on the database.

\ |IMG3|\ 


Deleting a variable
---------------------

Variables can be deleted from the list by clicking on the ``trash`` icon. A confirmation is needed.

\ |IMG4|\ 

Search a variable
-----------------------------------

Variables can be searched by name, device class & device name in the search box in top left corner.

\ |IMG2|\ 

Using Dashboard variables
-----------------------------------

Dashboard variable is created from `settings` button. It is possible to utilize these variables while creating widgets in device inspector. 

Device name of these variables can be changed by using the ``Variable Selector`` widget. In the ``Variable Selector`` widget, the  variable can be linked as below.

\ |IMG5|\ 

Once variable is configured, on run mode variables device name  can be changed as below

\ |IMG6|\ 

This device name change broadcasted to all the widgets subscribed to this variable.

Check the variable consistency
------------------------------

Taranta is able to check if the variables configured or imported are consistent with the devices running in the TangoDB. 

In the Dashboard list, a warning icon informs the user that there is a problem with the current configuration. 

\ |IMG7|\ 

Clicking the icon, in the variable list is shown the problem associated with the device. 

If the message associated with the device is: 'Device not running', it means that the device is registered in the TangoDB, but is not running.

\ |IMG8|\ 

If the message associated with the device is: 'Device not exported', it means that the device is not registered in the TangoDB.

\ |IMG9|\

The user is able to change the configuration of the variable with the devices running. It is not possible to run a Dashboard if there are any widgets configured with the variables that have a warning 


.. bottom of content

.. |IMG1| image:: _static/img/config_variable.png
   :height: 75 px
   :width: 350 px

.. |IMG2| image:: _static/img/manipulate_variable.png
   :height: 480 px


.. |IMG3| image:: _static/img/edit_variable.png
   :height: 80 px

.. |IMG4| image:: _static/img/delete_variable.png
   :height: 80 px

.. |IMG5| image:: _static/img/parametric_edit.png
   :height: 180 px

.. |IMG6| image:: _static/img/parametric_run.png
   :height: 180 px

.. |IMG7| image:: _static/img/invalid_configuration.png
   :height: 180 px

.. |IMG8| image:: _static/img/variable_device_not_running.png
   :height: 180 px

.. |IMG9| image:: _static/img/variable_device_not_exported.png
   :height: 180 px

