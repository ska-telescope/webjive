Macro Button
**************

The widget allows a user to run sardana macro execution from Taranta. It provides a graphical interface to configure macro execution settings(parameters).

Widget setting 
===============

Three devices are required to be configured in order to run a macro. 

+---------------+---------------------------------------------------------+
|Device         |Description                                              |
+===============+=========================================================+
|Door           |Specify the device to run macro execution                |
+---------------+---------------------------------------------------------+
|Macroserver    |Specify the device to show all available macros          |                   
+---------------+---------------------------------------------------------+
|Pool           |Specify the device to show all available motors          |
+---------------+---------------------------------------------------------+


\ |IMG1|\

In run mode
===============
The macro and its argument can be configured in running mode. There is a dropdown list with all available macros. \
If argument is required for the selected macro, the corresponding argument input box will pop up with description. \
The full macro argument are translated to spock syntax and shown next to the selected macro. If input wrong parameters, a warning message will show up.

To run or stop a macro execution, click Start/Stop button. The running output is presented under the execution button.

\ |IMG2|\


.. bottom of content

.. |IMG1| image:: _static/img/macrobutton_widget_setting.png
   :height: 197 px
   :width: 297 px

.. |IMG2| image:: _static/img/macrobutton_widget_running.png
   :height: 271 px
   :width: 516 px