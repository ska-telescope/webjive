Sample SVG with Format & Rules
==============================

.. code-block:: html

    <?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <!-- Created with Inkscape (http://www.inkscape.org/) -->

    <svg
    width="1200"
    height="900"
    viewBox="0 0 317.49999 238.125"
    version="1.1"
    id="svg1"
    sodipodi:docname="sun.svg"
    inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
    xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
    xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:svg="http://www.w3.org/2000/svg">
    <sodipodi:namedview
        id="namedview1"
        pagecolor="#ffffff"
        bordercolor="#000000"
        borderopacity="0.25"
        inkscape:showpageshadow="2"
        inkscape:pageopacity="0.0"
        inkscape:pagecheckerboard="0"
        inkscape:deskcolor="#d1d1d1"
        inkscape:document-units="px"
        inkscape:zoom="0.71446409"
        inkscape:cx="391.90213"
        inkscape:cy="561.25984"
        inkscape:window-width="1920"
        inkscape:window-height="991"
        inkscape:window-x="-9"
        inkscape:window-y="-9"
        inkscape:window-maximized="1"
        inkscape:current-layer="layer1" />
    <defs
        id="defs1" />
    <g
        inkscape:groupmode="layer"
        id="layer5"
        inkscape:label="main">
        <g
        inkscape:groupmode="layer"
        id="star"
        inkscape:label="star">
        <text
            xml:space="preserve"
            style="font-size:11.2889px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#aac800;fill-opacity:1;fill-rule:evenodd;stroke:#235213;stroke-width:0.469635;stroke-linejoin:bevel;paint-order:stroke fill markers"
            x="204.35498"
            y="100.94588"
            id="text3"
            inkscape:label="mystartext"><tspan
            sodipodi:role="line"
            id="tspan3"
            style="stroke-width:0.469635"
            x="204.35498"
            y="100.94588" /><tspan
            sodipodi:role="line"
            style="fill:#d40000;stroke-width:0.469635"
            x="204.35498"
            y="100.94588"
            id="tspan4">MyStar</tspan></text>
        <path
            sodipodi:type="star"
            style="fill:#00ffff;fill-rule:evenodd;stroke:#235213;stroke-width:0.469635;stroke-linejoin:bevel;paint-order:stroke fill markers"
            id="path1"
            inkscape:flatsided="false"
            sodipodi:sides="5"
            sodipodi:cx="192.56857"
            sodipodi:cy="39.25436"
            sodipodi:r1="49.783924"
            sodipodi:r2="13.831459"
            sodipodi:arg1="0.78601594"
            sodipodi:arg2="1.4143345"
            inkscape:rounded="0"
            inkscape:randomized="0"
            d="M 227.74937,74.478651 194.72385,52.916865 169.93975,83.59819 180.24078,45.526096 143.40239,31.436063 182.7943,29.468001 184.81103,-9.921443 198.85554,26.934322 236.94033,16.680339 206.2284,41.426517 Z"
            inkscape:transform-center-x="2.3972123"
            inkscape:transform-center-y="-2.4159865"
            transform="translate(22.244558,17.347691)"
            inkscape:label="mystar">
            <desc
            id="desc1">model=sys/tg_test/1</desc>
        </path>
        </g>
        <g
        inkscape:groupmode="layer"
        id="layer3"
        inkscape:label="Layer3">
        <rect
            style="fill:#d40000;fill-opacity:1;fill-rule:evenodd;stroke:#235213;stroke-width:0.469635;stroke-linejoin:bevel;paint-order:stroke fill markers"
            id="rect4"
            width="134.05736"
            height="21.849127"
            x="18.886532"
            y="107.02369" />
        <text
            xml:space="preserve"
            style="font-size:11.2889px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#d40000;fill-opacity:1;fill-rule:evenodd;stroke:#235213;stroke-width:0.469635;stroke-linejoin:bevel;paint-order:stroke fill markers"
            x="40.221451"
            y="117.94825"
            id="text5"><tspan
            sodipodi:role="line"
            id="tspan5"
            style="fill:#ff00ff;stroke-width:0.469635"
            x="40.221451"
            y="117.94825">Layer3</tspan></text>
        </g>
        <path
        sodipodi:type="star"
        style="display:inline;opacity:0.702213;fill:#c2d21a;fill-opacity:1;fill-rule:evenodd;stroke:#193900;stroke-width:0.469635;stroke-linejoin:bevel;paint-order:stroke fill markers"
        id="path2"
        inkscape:flatsided="false"
        sodipodi:sides="14"
        sodipodi:cx="73.694504"
        sodipodi:cy="19.62718"
        sodipodi:r1="42.223457"
        sodipodi:r2="17.969776"
        sodipodi:arg1="0.90975316"
        sodipodi:arg2="1.1683016"
        inkscape:rounded="0"
        inkscape:randomized="0"
        d="M 99.617199,52.95636 80.733533,36.160931 82.589056,60.903169 72.862725,37.577695 63.799238,60.674763 65.15666,35.43914 46.969299,52.316379 59.14162,30.168832 35.432614,37.483499 56.008957,22.810621 31.474166,19.113956 56.379134,14.82189 35.877973,0.84606271 60.178832,7.7849057 47.771809,-13.702 66.655474,3.0934292 64.799952,-21.648809 74.526283,1.6766648 83.58977,-21.420403 82.232348,3.8152202 100.41971,-13.062019 88.247388,9.0855281 111.95639,1.7708612 91.38005,16.443739 115.91484,20.140405 91.009874,24.43247 111.51103,38.408297 87.210176,31.469454 Z"
        inkscape:transform-center-x="0.32781589"
        inkscape:transform-center-y="-2.4068836"
        transform="matrix(0.76788633,0,0,0.66418599,31.567945,23.271687)"
        inkscape:label="sun" />
        <g
        inkscape:label="background"
        inkscape:groupmode="layer"
        id="layer1"
        style="display:inline">
        <rect
            style="display:inline;opacity:0.702213;fill:#107600;fill-opacity:1;fill-rule:evenodd;stroke:#193900;stroke-width:1.27738;stroke-linejoin:bevel;paint-order:stroke fill markers"
            id="rect1"
            width="316.45743"
            height="74.139801"
            x="0.40387398"
            y="163.34651"
            ry="0" />
        <rect
            style="display:inline;opacity:0.702213;fill:#12765b;fill-opacity:1;fill-rule:evenodd;stroke:#193900;stroke-width:0.535543;stroke-linejoin:bevel;paint-order:stroke fill markers"
            id="rect2"
            width="316.71594"
            height="162.9657"
            x="0.92119294"
            y="0.38081297" />
        <text
            xml:space="preserve"
            x="11.850371"
            y="192.74916"
            id="text1"><desc
            id="desc2">model=sys/tg_test/1/double_scalar
    format=numeral(0.0)
    format=concat(value:)
    </desc><tspan
            sodipodi:role="line"
            id="tspan1"
            style="stroke-width:0.469635"
            x="11.850371"
            y="192.74916">double scalar</tspan></text>
        <text
            xml:space="preserve"
            x="10.911868"
            y="208.49664"
            id="text1-5"
            style="display:inline"><desc
            id="desc2-74">model=sys/tg_test/1/short_scalar
    format=numeral(0.0)
    format=concat($:)</desc><tspan
            sodipodi:role="line"
            id="tspan1-7"
            style="stroke-width:0.469635"
            x="10.911868"
            y="208.49664">short scalar</tspan></text>
        <text
            xml:space="preserve"
            style="font-size:11.2889px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;display:inline;fill:#0000ff;fill-rule:evenodd;stroke:#235213;stroke-width:0.469635;stroke-linejoin:bevel;paint-order:stroke fill markers"
            x="11.850371"
            y="172.42603"
            id="text1-7"><desc
            id="desc2-7">model=sys/tg_test/1/double_spectrum_ro
    format=index(1)
    format=numeral(0.0)</desc><tspan
            sodipodi:role="line"
            id="tspan1-3"
            style="stroke-width:0.469635"
            x="11.850371"
            y="172.42603">double_ro</tspan></text>
        <text
            xml:space="preserve"
            x="11.850371"
            y="157.23624"
            id="text1-7-4"><desc
            id="desc2-7-9">model=test/tarantatestdevice/1/processorinfo
    format=/temperature/
    format=numeral(0.00)
    </desc><tspan
            sodipodi:role="line"
            id="tspan1-3-7"
            style="stroke-width:0.469635"
            x="11.850371"
            y="157.23624">ProcessorInfo</tspan></text>
        <rect
            style="fill:#b3b3b3"
            id="rules"
            width="19.997505"
            height="21.391819"
            x="-35.921448"
            y="63.782745"
            inkscape:label="rules">
            <desc
            id="desc3">rule1: {
        type=layer
            model=sys/tg_test/1/short_scalar
            layer=star
            condition=value &gt; 21 and value&lt;90
        }
    rule2: {
        type=css
            model=sys/tg_test/1/double_scalar
            ifcss={&quot;stroke&quot;:&quot;red&quot;}
            elsecss={&quot;stroke&quot;:&quot;yellow&quot;}
            condition=value &gt; 31
    }
    rule3: {
        type=css
            model=sys/tg_test/1/short_scalar
            ifcss={&quot;stroke&quot;:&quot;orange&quot;}
            elsecss={&quot;stroke&quot;:&quot;purple&quot;}
            condition=value&gt;31
    }</desc>
        </rect>
        </g>
    </g>
    </svg>
