SVG Widget
**********

The SVG widget allows rendering an SVG file in a dashboard. With SVG user can create his own design & layout depending on his needs.
Also down the line, the user can change the SVG depending on their needs. 

Widget setting 
==============

+---------------------+------------------------------------------------------------------------+
|Input value          |Description                                                             |
+=====================+========================================================================+
|Title                |The title to be given to the widget.                                    |
+---------------------+------------------------------------------------------------------------+
|SVG File             |Where you can upload the SVG file. In this case, clicking Browse will   |
|                     |open a window that allows you to upload only SVG files from your file   |
|                     |system. Once uploaded, the name of the loaded file will appear.         |
+---------------------+------------------------------------------------------------------------+
|SVG Uploaded         |Shows the name of svg file which is uploaded by user to the widget.     |
+---------------------+------------------------------------------------------------------------+
|Custom CSS           |Custom CSS to be applied to the widget.                                 |
+---------------------+------------------------------------------------------------------------+


Widget design
=============
This widget is used to render an SVG in taranta dashboard. A taranta dashboard can contain SVG Widget as like other widgets.

.. _create-svg-file:

Creating SVG file using Inkscape
--------------------------------

User can create SVG file using any SVG editor in our case its :ref:`inkscape-label`.
The SVG created is static, we need to link SVG elements to tango device / attributes to
make it dynamic. This linking part can be done through Inkscape. Follow :ref:`link-devices-on-svg`
for linking devices and attributes to SVG.  

SVG Widget in run mode
----------------------

Once SVG file is ready, it can be uploaded from widget configuration (SVG File), after uploading file on
SVG Widget it will render in the widget area. To see the actual device status and attribute values user
has to go in run mode by clicking the run button. All the SVG elements that are linked to device or device status will
change its color depending upon device status. The color of mapping of device status to color can be seen
`here <https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/built-in/atk/atk.html#devstate-scalar-attributes>`_,
at present this is not customizable. Also the SVG elements that are linked to device attributes will
pull real time attribute values and show up as shown below. The SVG elements that are not linked to any device will render
as it is.


\ |IMG3|\ 


This widget gives the user capability to create his custom design for dashboard hence the SVG file design can
be as simple to as complex depending upon the user requirement. A SVG demonstrating device status across different devices can be seen below:

\ |IMG4|\ 


.. bottom of content

.. |IMG3| image:: _static/img/svg-run-mode.png

.. |IMG4| image:: _static/img/svg-example.png
